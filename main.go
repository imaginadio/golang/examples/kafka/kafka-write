package main

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
	record "gitlab.com/imaginadio/golang/examples/kafka/kafka-record"
)

func main() {
	var (
		MIN, MAX, TOTAL, partition int
		topic                      string
	)

	if len(os.Args) != 5 {
		fmt.Printf("Usage: %s MIN MAX TOTAL TOPIC\n", os.Args[0])
		return
	}

	MIN, _ = strconv.Atoi(os.Args[1])
	MAX, _ = strconv.Atoi(os.Args[2])
	TOTAL, _ = strconv.Atoi(os.Args[3])
	topic = os.Args[4]
	r := rand.New(rand.NewSource(time.Now().Unix()))
	random := func() int {
		return r.Intn(MAX-MIN) + MIN
	}

	ctx := context.Background()
	conn, err := kafka.DialLeader(ctx, "tcp", "localhost:9092", topic, partition)
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	for i := 0; i < TOTAL; i++ {
		rnd := random()
		rec := record.Record{
			Name:   strconv.Itoa(i),
			Random: rnd,
		}
		recBytes, err := json.Marshal(rec)
		if err != nil {
			panic(err)
		}

		conn.SetWriteDeadline(time.Now().Add(time.Second))
		conn.WriteMessages(kafka.Message{Value: recBytes})
	}
}
