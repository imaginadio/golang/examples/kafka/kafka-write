module gitlab.com/imaginadio/golang/examples/kafka/kafka-write

go 1.20

require (
	github.com/segmentio/kafka-go v0.4.39
	gitlab.com/imaginadio/golang/examples/kafka/kafka-record v0.0.0-20230328080834-0e82f3412b03
)

require (
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/pierrec/lz4/v4 v4.1.15 // indirect
)
